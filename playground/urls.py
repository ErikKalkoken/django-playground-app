from django.urls import path

from . import views
from .views import ReviewCreateView

app_name = "playground"

urlpatterns = [
    path("", views.index, name="index"),
    path("create_review", ReviewCreateView.as_view(), name="review-create"),
]
