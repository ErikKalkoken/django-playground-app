from django.db import models
from django.urls import reverse


class Review(models.Model):
    """Review for an app"""

    rating = models.PositiveIntegerField()
    summary = models.CharField(max_length=255)

    def get_absolute_url(self):
        return reverse("index")
