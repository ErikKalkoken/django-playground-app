from django_starfield import Stars

from django import forms

from .models import Review


class ReviewForm(forms.ModelForm):
    rating = forms.IntegerField(widget=Stars)

    class Meta:
        model = Review
        fields = ("summary", "rating")
